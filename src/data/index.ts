import { CircuitsData } from './data-circuits';
import { StandingData } from './data-standing';
import { PilotsData } from './data-pilots';
import { RacesData } from './data-races';
import { SeasonsData } from './data-seasons';
export const data = {
	SeasonsData,
	RacesData,
	PilotsData,
	StandingData,
	CircuitsData
};
