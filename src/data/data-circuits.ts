import { F1 } from './data-source';

export class CircuitsData extends F1 {
	constructor() {
		super();
	}

	async getAllCircuits(pageElements: number = -1, page: number = 1) {
		if (pageElements === -1) {
			return await this.get('drivers.json?limit=1000', {
				cacheOptions: { ttl: 30 }
			});
		}
		const offset = (page - 1) * pageElements;
		const limit = pageElements;
		const filter = `limit=${limit}&offset=${offset}`;
		return await this.get(`circuits.json?${filter}`, {
			cacheOptions: { ttl: 30 }
		});
	}

	async getCircuitById(id: String) {
		return await this.get(`circuits/${id}.json`, {
			cacheOptions: { ttl: 30 }
		});
	}
}
