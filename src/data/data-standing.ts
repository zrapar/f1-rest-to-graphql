import { F1 } from './data-source';
import { checkStanding } from '../lib/utils';

export class StandingData extends F1 {
	constructor() {
		super();
	}

	async getStandingBySeason(season: String) {
		season = checkStanding(season);

		return await this.get(`${season}/driverStandings.json`, {
			cacheOptions: { ttl: 30 }
		});
	}
}
