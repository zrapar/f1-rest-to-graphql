import { F1 } from './data-source';
import { checkYear, roundCheck } from '../lib/utils';

export class RacesData extends F1 {
	constructor() {
		super();
	}

	// async getSeasons() {
	//   return await this.get('seasons.json?limit=100', {
	//     cacheOptions: { ttl: 30 }
	//   });
	// }

	async getRacesByYear(year: String) {
		year = checkYear(year);
		return await this.get(`${year}.json?limit=100`, {
			cacheOptions: { ttl: 30 }
		});
	}

	async getRacesByYearRound(year: String, round: Number) {
		year = checkYear(year);
		round = roundCheck(round);
		return await this.get(`${year}/${round}.json?limit=100`, {
			cacheOptions: { ttl: 30 }
		});
	}
}
