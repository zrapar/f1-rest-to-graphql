import { F1 } from './data-source';
import { checkYear, roundCheck } from '../lib/utils';

export class PilotsData extends F1 {
	constructor() {
		super();
	}

	async getAllPilots(pageElements: number = -1, page: number = 1) {
		if (pageElements === -1) {
			return await this.get('drivers.json?limit=1000', {
				cacheOptions: { ttl: 30 }
			});
		}
		const offset = (page - 1) * pageElements;
		const limit = pageElements;
		const filter = `limit=${limit}&offset=${offset}`;
		return await this.get(`drivers.json?${filter}`, {
			cacheOptions: { ttl: 30 }
		});
	}

	async getDriversBySeason(season: String) {
		season = checkYear(season);
		return await this.get(`${season}/drivers.json?limit=100`, {
			cacheOptions: { ttl: 30 }
		});
	}
	async getDriversBySeasonRound(season: String, round: Number) {
		season = checkYear(season);
		round = roundCheck(round);
		return await this.get(`${season}/${round}/drivers.json?limit=100`, {
			cacheOptions: { ttl: 30 }
		});
	}
	async getDriverById(id: String) {
		return await this.get(`drivers/${id}.json`, {
			cacheOptions: { ttl: 30 }
		});
	}
}
