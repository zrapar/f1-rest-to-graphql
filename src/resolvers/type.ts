import { IResolvers } from 'graphql-tools';
import { getWikipediaMobileUrl } from '../lib/utils';

const type: IResolvers = {
	Season: {
		year: (parent) => parent.season,
		urlMobile: (parent) => getWikipediaMobileUrl(parent.url)
	},
	Race: {
		name: (parent) => parent.raceName,
		circuit: (parent) => parent.Circuit,
		time: (parent) => (parent.time ? parent.time : 'Not defined'),
		urlMobile: (parent) => getWikipediaMobileUrl(parent.url)
	},
	Circuit: {
		id: (parent) => parent.circuitId,
		name: (parent) => parent.circuitName,
		location: (parent) => parent.Location,
		urlMobile: (parent) => getWikipediaMobileUrl(parent.url)
	},
	Location: {
		lng: (parent) => parent.long
	},
	Pilot: {
		id: (parent) => parent.driverId,
		urlMobile: (parent) => getWikipediaMobileUrl(parent.url),
		fullName: (parent) => `${parent.givenName} ${parent.familyName}`,
		birthDate: (parent) => parent.dateOfBirth,
		code: (parent) => (parent.code ? parent.code : ''),
		permanentNumber: (parent) => (parent.permanentNumber ? parent.permanentNumber : '')
	},
	Standing: {
		positionByPilot: (parent) => parent.DriverStandings
	},
	DriverStanding: {
		piloto: (parent) => parent.Driver,
		escuderia: (parent) => parent.Constructors[0]
	},
	Escuderia: {
		urlMobile: (parent) => getWikipediaMobileUrl(parent.url)
	}
};

export default type;
