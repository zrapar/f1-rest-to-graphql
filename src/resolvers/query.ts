import { IResolvers } from 'graphql-tools';

const query: IResolvers = {
	Query: {
		async seasonsList(__: void, _: any, { dataSources }) {
			return await dataSources.seasons.getSeasons().then((data: any) => data.MRData.SeasonTable.Seasons);
		},
		async racesByYear(__: void, { year }, { dataSources }) {
			return await dataSources.races.getRacesByYear(year).then((data: any) => data.MRData.RaceTable.Races);
		},
		async raceSelect(__: void, { year, round }, { dataSources }) {
			return await dataSources.races
				.getRacesByYearRound(year, round)
				.then((data: any) => data.MRData.RaceTable.Races[0]);
		},
		async allPilots(__: void, { pageElements, page }, { dataSources }) {
			return await dataSources.pilots
				.getAllPilots(pageElements, page)
				.then((data: any) => data.MRData.DriverTable.Drivers);
		},
		async driversBySeason(__: void, { season }, { dataSources }) {
			return await dataSources.pilots
				.getDriversBySeason(season)
				.then((data: any) => data.MRData.DriverTable.Drivers);
		},
		async driversBySeasonAndRound(__: void, { season, round }, { dataSources }) {
			return await dataSources.pilots
				.getDriversBySeasonRound(season, round)
				.then((data: any) => data.MRData.DriverTable.Drivers);
		},
		async driverSelect(__: void, { id }, { dataSources }) {
			return await dataSources.pilots.getDriverById(id).then((data: any) => data.MRData.DriverTable.Drivers[0]);
		},
		async standingsSeason(__: void, { season }, { dataSources }) {
			return await dataSources.standing
				.getStandingBySeason(season)
				.then((data: any) => data.MRData.StandingsTable.StandingsLists[0]);
		},
		async allCircuits(__: void, { pageElements, page }, { dataSources }) {
			return await dataSources.circuits
				.getAllCircuits(pageElements, page)
				.then((data: any) => data.MRData.CircuitTable.Circuits);
		},
		async circuitSelect(__: void, { id }, { dataSources }) {
			return await dataSources.circuits
				.getCircuitById(id)
				.then((data: any) => data.MRData.CircuitTable.Circuits[0]);
		}
	}
};

export default query;
