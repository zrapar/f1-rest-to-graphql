export function getWikipediaMobileUrl(url: String) {
	return url !== undefined ? url.replace('wikipedia', 'm.wikipedia') : '';
}

export function checkYear(year: String) {
	const currentYear = new Date().getFullYear();
	if (isNaN(+year) || +year < 1950 || +year > currentYear) {
		year = String(currentYear);
	}
	return year;
}

export function checkStanding(year: String) {
	const currentYear = new Date().getFullYear() - 1;
	if (isNaN(+year) || +year < 1950 || +year > currentYear) {
		year = String(currentYear);
	}
	return year;
}

export function roundCheck(round: Number) {
	if (round >= 100 || round === undefined) {
		return 1;
	}
	return round;
}
